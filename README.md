# README #

### Membres du groupe ###

* Anthony Rimet
* David Cordier

### Qu'est ce que le Mini-Framework ? ###

* Mini-Framework HTML/CSS dans le cadre d'un projet de la LP-CISIIE de l'IUT Charlemagne de Nancy. 

* C'est un Mini-Framework qui utilise les langages suivants: HTML/CSS/Sass/Compass.

* Appliqué à une page HTML, il permettra de la rendre responsive (s'adapte aux différentes tailles d'écran) et fera un reset au niveau de l'apparence pour appliquer ses propres choix (tableaux, boutons, balises).

### Présentation du Mini-Framework ###

Voici la liste des changements effectués par le Mini-Framework au niveau de l'apparence:

* Les titres
* Les sous-titres
* Les paragraphes
* Les balises de texte inline
* Les quote et blockquote
* Les listes
* Les alignements et transformations
* Les tableaux
* Les boutons
* Les alertes
* Les headers
* Les jumbotrons
* Les barres de navigation
* Les fils d'ariane
* Les paginations

Le mini-framework utilise sass pour améliorer sa flexibilité. Il utilise aussi compass pour importer des mixins pour la gestion de la grille (semblable à la grille du framework Bootstrap)

### Installation du Mini-Framework ###

* Dans le répertoire du bitbucket, on pourra retrouver un index.html qui permet de voir les différentes fonctionnalités du framework.

* Pour l'installer à son propre projet, il suffit de retirer l'index.html et de faire appel à style.css et custom.css dans le code de sa page HTML.

* Pour l'utilisation des balises et de la grille du framework, suivre l'exemple donné dans l'index.html.

### Exemple d'utilisation du Mini-Framework ###

Dans le cadre d'un autre projet pour le cours d'HTML Avancé, nous avons réalisé un site internet utilisant ce framework : 

* https://bitbucket.org/arimet/projet-html

### Lien du Framework ###

* https://webetu.iutnc.univ-lorraine.fr/www/rimet1u/Mini-FrameWork/
